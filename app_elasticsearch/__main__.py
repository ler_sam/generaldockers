import requests
import json


def request_header() -> dict:
    return {
        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) "
                      "Chrome/96.0.4664.45 Safari/537.36",
        "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
        "Accept-Language": "en-US,en;q=0.8",
        "Content-Type": "application/json",
        "Accept-Encoding": "gzip, deflate, br"}


if __name__ == '__main__':
    query = '{"query": {"match": {"FlightNum":"9HY9SWR"}}}'
    site_url = "http://127.0.0.1:9200/kibana_sample_data_flights/_search"
    s = requests.Session()
    req = requests.Request(method='GET', url=site_url, headers=request_header(), data=query)

    prepped = s.prepare_request(req)
    resp = s.send(prepped)
    if resp.status_code == requests.codes.ok:
        if resp.encoding in ['UTF-8', 'utf-8']:
            page_data = resp.text
        else:
            page_data = resp.text.decode("utf8")

        page_data = json.loads(page_data)
        print(page_data)
