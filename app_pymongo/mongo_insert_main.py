import json
import random
import string
import logging

from module import utils

logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
logger = logging.getLogger(__name__)


def get_random_string(length: int) -> str:
    # choose from all lowercase letter
    letters = string.ascii_lowercase
    result_str = ''.join(random.choice(letters) for i in range(length))
    return result_str


def create_object() -> dict:
    return {
        "first": get_random_string(5),
        "last": get_random_string(6)
    }


if __name__ == '__main__':
    client, collection = utils.get_db_handle('test_many')
    data = []
    for _ in range(30):
        data.append(create_object())
    print(json.dumps(data))
    result = collection.insert_many(data)
    print(result.acknowledged)
    data.clear()
