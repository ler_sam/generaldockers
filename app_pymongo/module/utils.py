import os
from pymongo import MongoClient
from dotenv import load_dotenv

load_dotenv()


def get_db_handle(collection_name: str):
    database = os.environ.get("MONGODB_DATABASE")
    host = os.environ.get("MONGODB_HOST")
    port = int(os.environ.get("MONGODB_PORT"))
    user_name = os.environ.get("MONGODB_USERNAME")
    password = os.environ.get("MONGODB_PASSWORD")

    client = MongoClient(host=host, port=port, username=user_name,
                         password=password, authSource='admin', directConnection=True)
    db = client[database]
    return client, db[collection_name]
