import logging
from module import utils
import threading
import time

logging.basicConfig(level=logging.INFO,
                    format='%(asctime)s - %(name)s - %(levelname)s - %(thread)d - %(threadName)s - %(message)s')
logger = logging.getLogger(__name__)

client, collection = utils.get_db_handle('collector')


def create_data():
    entry = {
        '_id': 1,
        'type': 'tiny',
        'input': 0,
        'output': 0,
        'insert_time': time.gmtime()
    }
    collection.insert_one(entry)


def update_counter_first():
    while True:
        results = collection.find_one_and_update(
            {'_id': 1},
            {'$inc': {'input': 1}}
        )
        logger.info(results)
        time.sleep(1)


def update_counter_second():
    while True:
        results = collection.find_one_and_update(
            {'_id': 1},
            {'$inc': {'input': 1}}
        )
        logger.info(results)
        time.sleep(.5)


def get_counter():
    while True:
        results = collection.find_one({'_id': 1})
        logger.info(results)
        time.sleep(.6)


if __name__ == '__main__':
    threading.Thread(target=update_counter_first,
                     args=(), name='first').start()
    threading.Thread(target=update_counter_second,
                     args=(), name='second').start()
    threading.Thread(target=get_counter,
                     args=(), name='third').start()
