import logging
from .module import utils

logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
logger = logging.getLogger(__name__)

client, user_collection = utils.get_db_handle('user')

entry = {
    '_id': 306505694,
    'first_name': 'Samuel',
    'last_name': 'Lerner',
    'email': 'LerSam@gmail.com'
}

entry_1 = {
    '_id': 306505637,
    'first_name': 'Frida',
    'last_name': 'Lerner',
    'email': 'LerFrida@gmail.com'
}

entry_2 = {
    '_id': 306505553,
    'first_name': 'Lev',
    'last_name': 'Lerner',
    'email': 'lerlev01@gmail.com'
}

user_collection.insert_one(entry)
user_collection.insert_many([entry_1, entry_2])

results = user_collection.find({'last_name': 'Lerner'})
for result in results:
    print(result)

print('---------------------')
result = user_collection.find_one({'_id': 306505694})
print(result)
print('---------------------')
user_collection.update_one({'_id': 306505694}, {'$set': {'first_name': 'Samuel'}})
user_count = user_collection.count_documents({'last_name': 'Lerner'})
print(user_count)
# https://www.youtube.com/watch?v=rE_bJl2GAY8
