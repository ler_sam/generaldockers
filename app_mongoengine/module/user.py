from mongoengine import *


class User(Document):
    user_id = LongField(primary_key=True)
    first_name = StringField(max_length=125)
    last_name = StringField(max_length=125)
    email = EmailField(max_length=125)
