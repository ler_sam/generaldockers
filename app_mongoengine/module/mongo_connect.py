import os

from mongoengine import connect
from dotenv import load_dotenv

load_dotenv()


def database_connector():
    database = os.environ.get("MONGODB_DATABASE")
    host = os.environ.get("MONGODB_HOST")
    port = int(os.environ.get("MONGODB_PORT"))
    user_name = os.environ.get("MONGODB_USERNAME")
    password = os.environ.get("MONGODB_PASSWORD")

    connect(db=database, host=host, port=port,
            username=user_name, password=password, authentication_source='admin', directConnection=True)
