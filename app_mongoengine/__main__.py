from .module.user import User
from .module.mongo_connect import database_connector

database_connector()

print('running')

try:
    # single select
    user = User.objects.get(user_id=306505694)
except User.DoesNotExist:
    # single inset
    user = User(
        user_id=306505694,
        first_name="Samuel",
        last_name="Lerner",
        email="LerSam@gmail.com")

    user.save()

entry = {
    'user_id': 306505694,
    'first_name': 'Samuel',
    'last_name': 'Lerner',
    'email': 'LerSam@gmail.com'
}

entry_1 = {
    'user_id': 306505637,
    'first_name': 'Frida',
    'last_name': 'Lerner',
    'email': 'LerFrida@gmail.com'
}

entry_2 = {
    'user_id': 306505553,
    'first_name': 'Lev',
    'last_name': 'Lerner',
    'email': 'lerlev01@gmail.com'
}

# insert bulk data
entries = [entry_1, entry_2]

person_instances = [User(**data) for data in entries]
User.objects.insert(person_instances, load_bulk=False)

# multiple select
users = User.objects(last_name="Lerner")
for user in users:
    print(user.first_name)

print('---------------------')
user_count = User.objects.count()
print(user_count)
