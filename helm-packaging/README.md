[TOC]
# Helm chart Basic 
**Add Helm Chart Repository:**
```bash
helm repo add bitnami https://charts.bitnami.com/bitnami
```
**Download the Chart:**
```bash
helm pull bitnami/mongodb
```
**Extract the Chart (Optional):**
```bash
tar -zxvf mongodb-<version>.tgz
```
**Installing:**
```bash
helm install basic-mongodb ./mongodb
```
----
# Minikube

**site:** [Minikube](https://minikube.sigs.k8s.io/docs/start/)

## Deployments

**execute minikube for hkube support**

```bash
minikube start \
--driver docker \
--addons ingress \
--addons registry \
--addons registry-aliases \
--addons storage-provisioner \
--listen-address=0.0.0.0
#--memory=max \
#--cpus=max \
#--disk-size=250g
```
**set docker-registry secret**
```bash
kubectl create secret docker-registry my-secret \
--docker-server=DOCKER_REGISTRY_SERVER \
--docker-username=DOCKER_USER \
--docker-password=DOCKER_PASSWORD \
--docker-email=DOCKER_EMAIL
```

**show minikube desktop**

```bash
nohup minikube dashboard &
```

**Get Minikube IP**

```bash
minikube ip
```

# [Hkube](hkube)

site: [hkube.io](https://hkube.io/learn/install/)

**temporary minikube changes**

After minikube is installed (before hkube is installed)

**set kubectl editor**
```bash
export KUBE_EDITOR=nano
```

**Run:**
```bash
kubectl -n ingress-nginx edit cm ingress-nginx-controller
 ```
**This allows you to edit config map, add the line in <span style="color: red">red</span>:**
<pre>
# Please edit the object below. Lines beginning with a '#' will be ignored,
# and an empty file will abort the edit. If an error occurs while saving this file will be
# reopened with the relevant failures.
#

apiVersion: v1
data:
  <span style="color: red">allow-snippet-annotations: "true"</span>
  hsts: "false"
kind: ConfigMap
~ 
</pre>

**Then run:**
```bash
kubectl  -n ingress-nginx delete ingress-nginx-controller
```

## Installing

```bash
helm install hkube \
--set build_secret.docker_registry=registry.minikube \
--set build_secret.docker_registry_insecure=true \
./helm-packaging/hkube
```

## Verify all running

```bash
kubectl rollout status deployment/simulator --watch; kubectl rollout status deployment/api-server --watch
```

**dashboard:** [http://192.168.49.2/hkube/dashboard/](http://192.168.49.2/hkube/dashboard/)

## Uninstalling

```bash
helm delete hkube
```
# [Consul](consul)

**site:** [Consul on Kubernetes](https://github.com/hashicorp/consul-k8s)

## Installing

```bash
helm install consul ./helm-packaging/consul \
--set global.name=consul \
--create-namespace -n consul
```
**dashboard:** [http://192.168.49.2:31080/ui/dc1/kv](http://192.168.49.2:31080/ui/dc1/kv)

## Uninstalling

```bash
helm delete consul
```
# [MongoDB](mongodb)

**site:** [MongoDB Bitnami Helm](https://artifacthub.io/packages/helm/bitnami/mongodb)

## Installing

```bash
helm install database ./helm-packaging/mongodb \
--set global.name=mongodb \
--create-namespace -n mongodb
```

To get database parameters for connection string 
```bash
export MONGODB_HOST=$(kubectl get svc --namespace mongodb database-mongodb -o jsonpath="{.spec.clusterIP}")
export MONGODB_PORT=$(kubectl get svc --namespace mongodb database-mongodb -o jsonpath="{.spec.ports[?(@.name==\"mongodb\")].port}")
export MONGODB_ROOT_PASSWORD=$(kubectl get secret --namespace mongodb database-mongodb -o jsonpath="{.data.mongodb-root-password}" | base64 -d)

echo $MONGODB_HOST
echo $MONGODB_PORT
echo $MONGODB_ROOT_PASSWORD

mongodb://root:<MONGODB_ROOT_PASSWORD>@<MONGODB_HOST>:<MONGODB_PORT>/?authSource=admin

```

## Uninstalling

```bash
helm delete database
```

# [Apache Kafka](kafka)

**site:** 

## Installing
```bash
helm install kafka-service ./helm-packaging/kafka \
--set global.name=kafka-service \
--create-namespace -n kafka-service
```

## Uninstalling

<!--
# [Kafka](kube-build/kafka)

**Installing**

```bash
kubectl create -f kube-build/kafka
```

**Service URL:**

```bash
minikube service --url kafka-nodeport-svc
```
---
## [kafka-ui](helm-packaging/kafka-ui)
```bash
helm install kafka-ui \
--set build_secret.docker_registry=registry.minikube \
--set build_secret.docker_registry_insecure=true \
./helm-packaging/kafka-ui
```
**Uninstalling**
```bash
helm delete kafka-ui
```
---
## [Mongo](kube-build/mongo)

**Installing (Stateful Sets)**

```bash
kubectl create -f kube-build/mongo
```

**Service URL:**

```bash
minikube service --url mongo-nodeport-svc -n product-site
```

## [Redis](kube-build/redis)

**Installing (Stateful Sets)**

```bash
kubectl create -f kube-build/redis
 ```
----
```

Consul - http://192.168.49.2:31080/ui/dc1/kv
Grafana - http://192.168.49.2:30001/?orgId=1
**Uninstalling**

```bash
helm delete consul
```
----
helm pull prometheus-community/kube-prometheus-stack --untar=true
helm pull  bitnami/mongodb --untar=true
helm pull  bitnami/elasticsearch --untar=true
helm pull  bitnami/kafka --untar=true
helm pull  bitnami/redis --untar=true


helm install prometheus ./helm-packaging/kube-prometheus-stack
helm upgrade prometheus ./helm-packaging/kube-prometheus-stack

kubectl get svc
/-->