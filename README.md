[TOC]

**Docker, Python, MongoDB, Redis, Hkube and Kubernetes**

_little bit of all_
# MongoDB
## MongoEngine
python support: [MongoEngine](https://docs.mongoengine.org/)

configuration [example](app_mongoengine)

```bash
python -m pip install mongoengine
```

**connector example:** [database connector](app_mongoengine/module/mongo_connect.py)

**usage example:**

- [Document module](app_mongoengine/module/user.py)
- [Module Operations](app_mongoengine/__main__.py)

## PyMongo
python support: [PyMongo](https://pymongo.readthedocs.io/en/stable/)

configuration [example](app_pymongo)

```bash
python -m pip install pymongo
```

**connector example:** [database connector](app_pymongo/module/utils.py)

**usage example:**

- [Module Operations](app_pymongo/mongo_insert_main.py)

## Docker
[MongoDB Bitnami packaged](https://hub.docker.com/r/bitnami/mongodb)
- **examples:**
  - [replica](replica/mongo)
  - [simple](simple/mongo)
<!--
**STARTING**

```bash
docker-compose up --detach \
--scale mongodb-primary=1 \
--scale mongodb-secondary=3 \
--scale mongodb-arbiter=1
```

**STOPING**

```bash
docker-compose -f mongo_replica.yml down
```
/-->
## Support Links

[Implementing pessimistic locks in MongoDB](https://faun.pub/implementing-pessimistic-locks-in-mongodb-8f3fbe2ddfa9)

---

# [Redis](app_redis/README.md)

Redis is an open source (BSD licensed), in-memory data structure store used as a database, cache, message broker, and
streaming engine. Redis provides data structures such as strings, hashes, lists, sets, sorted sets with range queries,
bitmaps, hyperloglogs, geospatial indexes, and streams. Redis has built-in replication, Lua scripting, LRU eviction,
transactions, and different levels of on-disk persistence, and provides high availability via Redis Sentinel and
automatic partitioning with Redis Cluster.

---

# Kafka
[Documentation](https://kafka.apache.org/documentation/)

configuration [example](kafka)

- [Docker](https://hub.docker.com/r/bitnami/kafka)
- [Kafka Python](https://kafka-python.readthedocs.io/en/master/index.html)
- [Dashboard](http://127.0.0.1:9000/)