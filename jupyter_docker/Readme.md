```bash
eval $(minikube docker-env)
docker build \
--tag jupyter \
.
```

```bash
docker run -it \
--detach \
--network host \
jupyter
```
```bash
eval $(minikube docker-env)
kubectl create -f kube-build.yaml
kubectl delete -f kube-build.yaml
```