from hkube_python_wrapper import Algorunner
from app.main import start

if __name__ == "__main__":
    Algorunner.Run(start=start)
