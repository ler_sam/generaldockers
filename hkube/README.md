Contents:

- [HKube  builds](#hkube--builds)
  - [Basic Usage (manual build)](#basic-usage-manual-build)
  - [Docker running entry command:](#docker-running-entry-command)
  - [HKube Listener](#hkube-listener)
- [TCP Socket Usage](#tcp-socket-usage)
  - [First pipline algorithm](#first-pipline-algorithm)


# HKube  builds
## Basic Usage (manual build)
The start method calls with the args parameter, the inputs to the algorithm will appear in the input property.
```commandline
from hkube_python_wrapper import Algorunner, HKubeApi


def start(args: dict, hkubeApi: HKubeApi):
    # cousumer call function
    print(args)
    return 1


if __name__ == "__main__":
    Algorunner.Run(start=start)
```
to receive args parameters, we need to know in which flow we're using:</br>
1. Stateless algorithm will receive params like this:
```commandline
params = args.get('streamInput')['message']
```
2. StateFull algorithm will receive params like this:
```commandline
params = args['input']
```
---
## Docker running entry command:

```commandline
CMD ["/bin/sh", "-c", "python -u app.py 2>&1 |tee /hkube-logs/stdout.log"]
```

app.py should include ``Algorunner.Run(start=start)``

---

## HKube Listener
To send message with HKube you should start listener
```commandline
hkubeApi.registerInputListener(onMessage=handle_message)
hkubeApi.startMessageListening()
```
and sending message will be done by

```commandline
hkubeApi.sendMessage(msg)
hkubeApi.sendMessage(msg, flowName=None)
```
**msg** - the massage should be json object</br>
**flowName** - pipeline flow name, used in case we have number of flows

---
# TCP Socket Usage

our main usage is by TCP Socket.</br>
This mean we doesn't use normal Hkube api calls.</br>
When we build our 'pipline' first algorithm should be TCP Socket client.

## First pipline algorithm
Basic TCP Client for example [realpython](https://realpython.com/python-sockets/)

```commandline

import socket

HOST = "127.0.0.1"  # Standard loopback interface address (localhost)
PORT = 65432  # Port to listen on (non-privileged ports are > 1023)

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.bind((HOST, PORT))
    s.listen()
    conn, addr = s.accept()
    with conn:
        print(f"Connected by {addr}")
        while True:
            data = conn.recv(1024)
            if not data:
                break
            hkubeApi.sendMessage(data)
```
as you can see from example above it's simple socket listener the only different is in line
```commandline
hkubeApi.sendMessage(message)
```
this line support sending received result to the next Hkube algorithm</br>
for this working you need to start Hkube listener in start of algorithm flow by execute:
```commandline
def handleMessage(msg, origin):
    pass

hkubeApi.registerInputListener(onMessage=handleMessage)
hkubeApi.startMessageListening()
```
basic example can be found in  [algorithm-example-python](https://github.com/kube-HPC/algorithm-example-python)