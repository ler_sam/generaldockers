import sys
from typing import AsyncIterator

import asyncpg
import asyncio
import uvloop

asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())


async def entrypoint() -> None:
    async with asyncpg.create_pool(
            "postgresql://postgres:postgres@localhost/postgres"
    ) as db:
        await print_all_users(db)


async def entrypoint_with_gen() -> None:
    async with asyncpg.create_pool(
            "postgresql://postgres:postgres@localhost/postgres"
    ) as db:
        async for record in gen_all_users(db):
            print(record)


async def gen_all_users(db: asyncpg.pool.Pool) -> AsyncIterator[asyncpg.Record]:
    async with db.acquire() as conn:
        async with conn.transaction():
            async for record in conn.cursor("SELECT * FROM pg_user"):
                yield record


async def print_all_users(db: asyncpg.pool.Pool) -> None:
    async with db.acquire() as conn:
        async with conn.transaction():
            async for record in conn.cursor("SELECT * FROM pg_user"):
                print(record)


if sys.version_info >= (3, 11):
    with asyncio.Runner(loop_factory=uvloop.new_event_loop) as runner:
        # runner.run(entrypoint())
        runner.run(entrypoint_with_gen())

else:
    uvloop.install()
    # asyncio.run(entrypoint())
    asyncio.run(entrypoint_with_gen())
