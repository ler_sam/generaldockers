import sys
import asyncio
import uvloop

from async_mongo.module import Connection

asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())

DNS = "mongodb://samuelle:lersam@0.0.0.0:27017/admin"


async def ping_server():
    """
    checking database connection by pimg to admin database
    """
    with Connection(DNS) as client:
        await client["admin"].command('ping')
        print("Pinged your deployment. You successfully connected to MongoDB!")


async def insert_one(document: dict):
    with Connection(DNS) as client:
        db = client['user']
        collection = db['my_collection']
        result = await collection.insert_one(document)
        print(f"Inserted document with ID: {result.inserted_id}")


async def insert_many(data: list):
    with Connection(DNS) as client:
        db = client['user']
        collection = db['my_collection']
        result = await collection.insert_many(data)
        print(f"Inserted document with ID: {result.inserted_ids}")


async def select_all():
    with Connection(DNS) as client:
        db = client['user']
        collection = db['my_collection']
        data = []
        async for doc in collection.find():
            data.append(doc)
        return data


async def async_main() -> None:
    document = [
        {
            'user_id': 306505694,
            'first_name': 'Samuel',
            'last_name': 'Lerner',
            'email': 'LerSam@gmail.com'
        }, {
            'user_id': 306505637,
            'first_name': 'Frida',
            'last_name': 'Lerner',
            'email': 'LerFrida@gmail.com'
        }, {
            'user_id': 306505553,
            'first_name': 'Lev',
            'last_name': 'Lerner',
            'email': 'lerlev01@gmail.com'
        }]
    try:
        await asyncio.wait_for(
            asyncio.gather(
                ping_server(),
                insert_one(document[0]),
                insert_many(document[1:])
            ), 5)
        data = await select_all()
        print(data)
    except asyncio.TimeoutError:
        print("oops, time's up")
    except KeyboardInterrupt:
        print("oops, Keyboard Interrupt")


if sys.version_info >= (3, 11):
    with asyncio.Runner(loop_factory=uvloop.new_event_loop) as runner:
        runner.run(async_main())
else:
    uvloop.install()
    asyncio.run(async_main())
