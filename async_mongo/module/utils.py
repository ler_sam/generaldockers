from dataclasses import dataclass
from typing import Optional
from motor.motor_asyncio import AsyncIOMotorClient
from pymongo.server_api import ServerApi


@dataclass
class Connection:
    dsn: str
    conn: Optional[AsyncIOMotorClient] = None

    def __enter__(self) -> AsyncIOMotorClient:
        self.conn = AsyncIOMotorClient(self.dsn, server_api=ServerApi('1'))
        return self.conn

    def __exit__(self, exc_type, exc_val, exc_tb):
        if self.conn:
            self.conn.close()
