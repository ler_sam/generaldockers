# Redis with Python

To use Redis with Python, you need a Python Redis client.

```bash
python -m pip install redis
```

## Connect to Redis

The following code creates a connection to Redis using redis-py:

```python
import redis

port = 63
r = redis.Redis(host='hostname', port=port, password='password')

```

**usage example can be found in:** [redis_examples.py](redis_examples.py)

---

# Official Redis Docker Image

[docker hub](https://hub.docker.com/_/redis)

Docker Official Images are a curated set of Docker open source and drop-in solution repositories.

## Working Redis master-slave example

base configuration 1 _**master**_ and 3 _**replica**_
- **example:**
    - [replica set](../replica/redis)
    - [simple](../simple/redis)
<!--
**STARTING**

```bash
docker-compose up --detach \
--scale redis-master=1 \
--scale redis-replica=3
```

**STOPING**

```commandline
docker-compose -f redis_replica.yml down
```
/-->
## Support Links

* [Redis with Python](https://docs.redis.com/latest/rs/references/client_references/client_python/)
* [Manage concurrency in multiple Python clients in Redis](https://medium.com/@e.ahmadi/manage-concurrency-in-multiple-python-clients-in-redis-5f9a05836a92)
* [Redis Command](https://redis.io/commands/)