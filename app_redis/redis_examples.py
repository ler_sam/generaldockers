import json

from dotenv import load_dotenv
from app_redis.module import utils
from app_redis.module import utilities as util

load_dotenv()

# redis_client = utils.RedisWrapper.INIT_REDIS()
redis_client = util.redis_connector()


def set_example():
    """
    Set key to hold the string value. If key already holds a value, it is overwritten, regardless of its type.
    Any previous time to live associated with the key is discarded on successful SET operation.

    Simple string reply: OK if SET was executed correctly.
    For more information see https://redis.io/commands/set/

    Return
    True if it sets the value successfully and False otherwise

    """
    # simple example
    status = redis_client.set(name="mykey", value="Hello")
    print(status)

    # example with expire  time(seconds)
    status = redis_client.set(name="exp_key", value="I will be gone 60 seconds after creation", ex=60)
    print(status)


def get_example():
    """
    Return the value at key ``name``, or None if the key doesn't exist
    For more information see https://redis.io/commands/get
    """

    # if you run  set_example(), return value will-be b'Hello'
    key_data = redis_client.get("mykey")
    print(key_data)

    # if you run  set_example(), return value will-be b`I will be gone 60 seconds after creation`
    # aftr 60 second return value will-be `None`
    key_data = redis_client.get("exp_key")
    print(key_data)


def sadd_example():
    """
    Add the specified members to the `set` stored at key.
    Specified members that are already a member of this set are ignored.
    If key does not exist, a new set is created before adding the specified members.

    An error is returned when the value stored at key is not a set.

    Return
    Integer reply: the number of elements that were added to the set,
    not including all the elements already present in the set.

    For more information see https://redis.io/commands/sadd/
    _________________________
    | ID (Total: N) | Value |
    _________________________
    """

    added_counter = redis_client.sadd("my_set_1", *['test', 'sdzsfasdgse'])
    print(added_counter)

    added_counter = redis_client.sadd('my_set_2', *set([3, 4]))
    print(added_counter)

    added_counter = redis_client.sadd("my_set_3", json.dumps({'aaaa': 'test', 'sefsfdgsedwe': 'sdzsfasdgse'}),
                                      json.dumps({'bbbb': 'test', 'cccccccccccc': 'sdzsfasdgse'}))
    print(added_counter)


def smembers_example():
    """
    Returns all the members of the set value stored at key.
    This has the same effect as running SINTER with one argument key.

    For more information see https://redis.io/commands/smembers
    """
    # if you run  sadd_example(), return value will-be {b'sdzsfasdgse', b'test'}
    set_data = redis_client.smembers("my_set_1")
    print(set_data)

    # if you run  sadd_example(), return value will-be {b'4', b'3'}
    set_data = redis_client.smembers("my_set_2")
    print(set_data)

    # if you run  sadd_example(), return value will-be
    # {b'{"bbbb": "test", "cccccccccccc": "sdzsfasdgse"}', b'{"aaaa": "test", "sefsfdgsedwe": "sdzsfasdgse"}'}
    set_data = redis_client.smembers("my_set_3")

    for item in set_data:
        print(json.loads(item))


def hset_example():
    """
    Set ``key`` to ``value`` within hash ``name``,
    ``mapping`` accepts a dict of key/value pairs that will be added to hash ``name``.
    ``items`` accepts a list of key/value pairs that will be added to hash ``name``.

    Returns the number of fields that were added.

    For more information see https://redis.io/commands/hset
    _______________________________
    | ID (Total: N) | Key | Value |
    -------------------------------
    """
    added_counter = redis_client.hset(name="my_hash_1", key="field1", value="Hello")
    print(added_counter)

    added_counter = redis_client.hset(name="my_hash_2", mapping={'aaaa': 'test', 'bbbb': 'sdzsfasdgse'})
    print(added_counter)

    # added_counter = redis_client.hset(name="my_hash_3", items={'aaaa': 'test', 'sefsfdgsedwe': 'sdzsfasdgse'})
    # print(added_counter)


def hget_example():
    """
    Returns the value associated with field in the hash stored at key.

    Return
    Bulk string reply: the value associated with field, or nil when field is not present in
    the hash or key does not exist.

    For more information see https://redis.io/commands/hget/
    """
    _data = redis_client.hget(name="my_hash_1", key="field1")
    print(_data)

    _data = redis_client.hget(name="my_hash_2", key="aaaa")
    print(_data)

    _data = redis_client.hget(name="my_hash_2", key="bbbb")
    print(_data)


def hgetall_example():
    """
    Returns all fields and values of the hash stored at key. In the returned value, every field
    name is followed by its value, so the length of the reply is twice the size of the hash.

    Return
    Array reply: list of fields and their values stored in the hash, or an empty list when key does not exist.

    For more information see https://redis.io/commands/hgetall
    """
    _data = redis_client.hgetall(name="my_hash_2")
    print(_data)
    return _data

def hgetall_example_with_simple_connector():
    _client = util.redis_connector()
    _data = _client.hgetall(name="my_hash_2")
    print(_data)
    return _data

if __name__ == '__main__':
    set_example()
    get_example()
    # ----------------------------------
    sadd_example()
    smembers_example()
    # ----------------------------------
    hset_example()
    hget_example()
    hgetall_example()
