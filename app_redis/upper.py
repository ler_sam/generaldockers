import time
import threading
from redis import exceptions

from app_redis.module import utils


def upper():
    """
    redis watch example
    """
    next_value = 0
    redis_connector = utils.RedisWrapper.INIT_REDIS()
    while True:
        with redis_connector.pipeline() as pipe:
            error_count = 0
            while True:
                try:
                    pipe.watch('counter')
                    current_value = int(pipe.get('counter')) if pipe.get('counter') else 0
                    next_value = current_value + 1
                    pipe.multi()
                    pipe.set('counter', next_value, None)
                    print(f"value: {next_value}")
                    pipe.execute()
                    break
                except  exceptions.WatchError:
                    # show which counter is used by another user
                    print(f"revoked: {next_value}")
                    error_count += 1
        print('-----')
        time.sleep(0.5)


if __name__ == '__main__':
    for index in range(10):
        threading.Thread(target=upper, name=f'thread-{index}').start()
