import os
import logging
from redis import Redis
from redis.connection import ConnectionPool

logger = logging.getLogger()

REDIS_CLIENT = None


def redis_connector() -> Redis:
    global REDIS_CLIENT

    if not REDIS_CLIENT:
        logger.info("Redis Initialization")

        _host = os.getenv("REDIS_HOST")
        _port = int(os.getenv("REDIS_PORT"))
        _password = os.getenv("REDIS_PASSWORD")
        _db = int(os.getenv("REDIS_DB"))

        redis_params = {"host": _host, "port": _port, "password": _password, "db": _db, "decode_responses": True}
        REDIS_CLIENT = Redis(connection_pool=ConnectionPool(**redis_params))

    return REDIS_CLIENT
