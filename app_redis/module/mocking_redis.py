from typing import Union


class MockingRedis:
    def __init__(self):
        self.redis_data = self._data()

    def hgetall(self, name: str) -> Union[dict, None]:
        return self.redis_data.get(name)

    def _data(self):
        return {
            "my_hash_2": "1111"
        }
