import os
import logging
from dotenv import load_dotenv

import redis

LOGGER = logging.getLogger()

load_dotenv()


class RedisWrapper:
    """
    represent Redis connector wrapper
    """
    RETRIES_COUNTER = 3

    def __init__(self, redis_host: str, redis_port: int, redis_db=0):
        self.redis_host = redis_host
        self.redis_port = redis_port
        self.redis_db = redis_db
        self.password = os.environ.get('REDIS_PASSWORD')
        self.manual_retries = 0

    def get_client(self) -> redis.Redis:
        """
        start and initialisation Redis client

        return redis.Redis
        """

        try:
            redis_client = redis.Redis(host=self.redis_host, port=self.redis_port, db=self.redis_db,
                                       password=self.password, retry_on_timeout=True)
        except redis.exceptions.ConnectionError:
            LOGGER.exception("Redis ConnectionError")

        except redis.exceptions.TimeoutError:
            LOGGER.exception("Redis TimeoutError")
        return redis_client

    @staticmethod
    def INIT_REDIS() -> redis.Redis:
        """
        performe class initialisation from Enviroment variables

        :return: redis.Redis
        """
        redis_host = os.environ.get('REDIS_HOST')
        redis_port = int(os.environ.get('REDIS_PORT'))
        redis_db = int(os.environ.get('REDIS_DB'))
        connector = RedisWrapper(redis_host=redis_host, redis_port=redis_port, redis_db=redis_db)

        return connector.get_client()
