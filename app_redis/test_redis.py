from unittest import mock

from app_redis.module.mocking_redis import MockingRedis
from app_redis.redis_examples import hgetall_example_with_simple_connector


@mock.patch("app_redis.module.utilities.redis_connector", new=MockingRedis)
def test_hgetall_example():
    result = hgetall_example_with_simple_connector()
    assert int(result) == 1111
